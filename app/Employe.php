<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    protected $guarded = [];

    public function section()
    {
        return $this->hasMany(Section::class);
    }

    public function salary()
    {
        return $this->hasMany(Salary::class);
    }
}
