<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Expenses;

class Tcategory extends Model
{
    protected $guarded = [];

    public function expense()
    {
        return $this->hasMany(Expenses::class);
    }
}
