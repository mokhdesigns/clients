<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sales;
use App\Client;

class Transaction extends Model
{
    protected $guarded = [];


    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function sale()
    {
        return $this->belongsTo(Sales::class);
    }
}
