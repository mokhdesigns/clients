<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $guarded = [];

    public function contract()
    {
        return $this->hasMany(Contract::class);
    }
}
