<?php

namespace App;
use App\Tcategory;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $guarded = [];

    public function tcategory()
    {
        return $this->belongsTo(Tcategory::class);
    }
}
