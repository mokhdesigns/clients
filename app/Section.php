<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $guarded = [];

    public function employe()
    {
        return $this->belongsTo(Employe::class);
    }
}