<?php

namespace App\Http\Controllers;

use App\Tcategory;
use Illuminate\Http\Request;

class TcategoryController extends Controller
{
    public function index()
    {
        $categories = Tcategory::all();

        return view('tcategory.index', compact('categories'));

    }


    public function create()
    {
        return view('tcategory.create');
    }


    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Tcategory::create($input);

        session()->flash('message', 'تمت اضافه الخدمه بنجاح ');

        return redirect('/tcategory');

    }


    public function edit($id)
    {
        $category = Tcategory::findOrFail($id);

        return view('tcategory.edit', compact('category'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Tcategory::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل الخدمه بنجاح ');

        return redirect('/category');

    }


    public function destroy($id)
    {
        Tcategory::findOrFail($id)->delete();

        return redirect('/tcategory');
    }
}
