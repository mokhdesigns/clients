<?php

namespace App\Http\Controllers;

use App\Sales;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function index()
    {
        $sales = Sales::all();

        return view('sales.index', compact('sales'));

    }


    public function create()
    {
        return view('sales.create');
    }


    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Sales::create($input);

        session()->flash('message', 'تمت اضافه الموظف بنجاح ');

        return redirect('/sale');

    }


    public function edit($id)
    {
        $sale = Sales::findOrFail($id);

        return view('sales.edit', compact('sale'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Sales::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل الخدمه بنجاح ');

        return redirect('/sale');

    }


    public function destroy($id)
    {
        Sales::findOrFail($id)->delete();

        return redirect('/sale');
    }
}
