<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employe;
use App\Section;

class EmployeController extends Controller
{

    public function index()
    {
        $employes = Employe::all();

        return view('employe.index', compact('employes'));
    }

    public function create()
    {
        $sections = Section::all();

        $employes = Employe::all();

        return view('employe.create', compact('employes', 'sections'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required',
            'salary'    => 'required',
            'cuts'      => '',
            'bouns'     => '',
            'address'   => 'required',
            'start_date'=> 'required',
            'personal'  => 'required'
        ]);

        $input = $request->all();

        Employe::create($input);

        session()->flash('message', 'تمت اضافه الموظف بنجاح ');

        return redirect('/employe');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $employe = Employe::findOrFail($id);

        $sections = Section::all();

        return view('employe.edit', compact('employe', 'sections'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name'      => 'required',
            'email'     => 'required',
            'salary'    => 'required',
            'cuts'      => '',
            'bouns'     => '',
            'address'   => 'required',
            'start_date'=> 'required',
            'personal'  => 'required'
        ]);

        $input = $request->all();

        Employe::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل بيانات الموظف بنجاح ');

        return redirect('/employe');
    }

    public function destroy($id)
    {
        Employe::findOrFail($id)->delete();

        return redirect('/employe');
    }
}
