<?php

namespace App\Http\Controllers;

use App\Category;
use App\Client;
use App\Contract;
use App\Sales;
use App\Transaction;
use Illuminate\Http\Request;
use DB;
class ContractController extends Controller
{

    public function index()
    {
        $contracts = Contract::all();

        return view('contract.index', compact('contracts'));
    }


    public function create()
    {
        $categories = Category::all();

        $clients = Client::all();

        $sales = Sales::all();


        return view('contract.create', compact('categories', 'clients', 'sales'));
    }


    public function store(Request $request)
    {


        $request->validate([

            'category_id' => 'required',
            'client_id'    => 'required',
            'personal_id'  => 'required',
            'total'        => 'required',
            'advance'      => 'required',
            'deadline'     => 'required',
            'body'         => 'required',

        ]);

        $input = $request->all();

        $input['own'] = $input['total'] -  $input['advance'];

        $file = $request->file('avatar');

        if($file){

             $fileName = $file->getClientOriginalExtension();

             $fileName = uniqid() . 'mokhtar_ali.' . $fileName;

             $file->move('images/', $fileName);

             $input['avatar'] = $fileName;

        }

        Contract::create($input);

        session()->flash('message', 'تمت اضافه العقد بنجاح ');

        return redirect('/contract');
    }


    public function show($id)
    {
        $contract = Contract::findOrFail($id);

        $transactions  = DB::table('transactions')
        ->select('*')
        ->join('contracts', 'contracts.client_id', '=', 'transactions.client_id')
        ->join('sales', 'sales.id', '=', 'transactions.sale_id')
        ->where('contracts.id', $id)
        ->get();


        return view('contract.show', compact('contract', 'transactions'));
    }

    public function edit($id)
    {
        $contract = Contract::findOrFail($id);

        $categories = Category::all();

        $clients = Client::all();

        $sales = Sales::all();

        return view('contract.edit', compact('contract','categories','clients','sales'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'client_id'    => 'required',
            'personal_id'  => 'required',
            'total'        => 'required',
            'advance'      => 'required',
            'deadline'     => 'required',
            'body'         => 'required',
        ]);

        $input = $request->all();

        $input['own'] = $input['total'] -  $input['advance'];

        $file = $request->file('avatar');

        if($file){

             $fileName = $file->getClientOriginalExtension();

             $fileName = uniqid() . 'mokhtar_ali.' . $fileName;

             $file->move('images/', $fileName);

             $input['avatar'] = $fileName;

        }else {

            $input['avatar'] = 'default.png';

        }

        Contract::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل العقد بنجاح ');

        return redirect('/contract');

    }


    public function destroy($id)
    {
        Contract::findOrFail($id)->delete();

        session()->flash('message', 'Contract Deleted Sucessfully.');

        return redirect('/contract');
    }

    public function transaction ($id) {

      $sales = Sales::all();
      return view('contract.add', compact('id', 'sales'));

    }


    public function addmoney (Request $request) {

   $request->validate([

    'amount'      => 'required',
    'sale_id'    => 'required',
    'client_id'   => 'required'
   ]);

   $input = $request->all();


      Transaction::create($input);


     $contract =  Contract::findOrFail($input['client_id'])->decrement('own', $input['amount']);

     session()->flash('message', 'تمت اضافه المبلغ  بنجاح ');

     return redirect('/contract');
    }
}
