<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses;
use App\Contract;
use DB;

class DetailsController extends Controller
{
    public function showexpenses()
    {

        $expenses  = Expenses::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months"),
        )
        ->groupBy('months')
        ->orderBy('created_at', 'ASC')
        ->get();



        return view('expensesdetails', compact('expenses'));
    }

    public function showcontract()
    {

        $contractstotal  = Contract::select(
            DB::raw('sum(total) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months"),

            )
            ->groupBy('months')
            ->get();

        return view('contractdetails', compact('contractstotal'));
    }


    public function showcategoryDetails($id)
    {



        $contractstotal  = Expenses::select(
            DB::raw('sum(amount) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months"),
            )->where('tcategory_id', '=', $id)
            ->groupBy('months')
            ->get();


        return view('contractdetails', compact('contractstotal'));
    }



    public function showsalescategoryDetails($id)
    {

        $contractstotal  = Contract::select(
            DB::raw('sum(total) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months"),
            )->where('category_id', '=', $id)
            ->groupBy('months')
            ->get();


        return view('contractdetails', compact('contractstotal'));
    }

}
