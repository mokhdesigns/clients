<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{

    public function index()
    {
        $clients = Client::all();

        return view('Client.index', compact('clients'));
    }


    public function create()
    {
        return view('client.create');
    }


    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required | min:3',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $input = $request->all();

        Client::create($input);

        session()->flash('message', 'تمت اضافه الخدمه بنجاح ');

        return redirect('/client');
    }



    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('client.edit', compact('client'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Client::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل الخدمه بنجاح ');

        return redirect('/client');
    }


    public function destroy($id)
    {
        Client::findOrFail($id)->delete();
        return redirect('/client');
    }
}
