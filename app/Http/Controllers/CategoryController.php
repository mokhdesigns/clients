<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();

        return view('category.index', compact('categories'));

    }


    public function create()
    {
        return view('category.create');
    }


    public function store(Request $request)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Category::create($input);

        session()->flash('message', 'تمت اضافه الخدمه بنجاح ');

        return redirect('/category');

    }


    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('category.edit', compact('category'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([

            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Category::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل الخدمه بنجاح ');

        return redirect('/category');

    }


    public function destroy($id)
    {
        Category::findOrFail($id)->delete();

        return redirect('/category');
    }
}
