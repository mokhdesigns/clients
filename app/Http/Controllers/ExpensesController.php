<?php

namespace App\Http\Controllers;

use App\Expenses;
use App\Tcategory;
use App\WalletTotal;
use Illuminate\Http\Request;

class ExpensesController extends Controller
{

    public function index()
    {
        $expenses = Expenses::latest()->get();

        return view('expenses.index', compact('expenses'));

    }


    public function create()
    {
        $categories = Tcategory::all();

        return view('expenses.create', compact('categories'));
    }


    public function store(Request $request)
    {
        $request->validate([

            'tcategory_id' => 'required',
            'amount'    => 'required',
            'body' => 'required'
        ]);

        $input = $request->all();

        Expenses::create($input);

        $total =   WalletTotal::findOrFail('1');

        $total->decrement('amount', $request->amount);

        session()->flash('message', 'تمت اضافه المصروف بنجاح ');

        return redirect('/expenses');
    }



    public function edit($id)
    {
        $expenses = Expenses::findOrFail($id);

        $categories = Tcategory::all();

        return view('expenses.edit', compact('expenses', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'tcategory_id' => 'required',
            'body'         => 'required'
        ]);

        $input = $request->all();

        Expenses::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل المصروف بنجاح ');

        return redirect('/expenses');
    }

    public function destroy($id)
    {
        Expenses::findOrFail($id)->delete();

        return redirect('/expenses');
    }
}
