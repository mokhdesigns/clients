<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contract;
use App\Expenses;
use App\Tcategory;
use App\WalletTotal;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $contractstotal  = Contract::select(
            DB::raw('sum(total) as sums'),
            DB::raw("DATE_FORMAT(created_at,'%M') as months")
            )
            ->groupBy('months')
            ->get();

            $expensestotal  = Expenses::select(
                DB::raw('sum(amount) as sums'),
                DB::raw("DATE_FORMAT(created_at,'%M') as months")
            )
            ->groupBy('months')
            ->orderBy('created_at', 'ASC')
            ->get();

        $expensessum = Expenses::sum('amount');

        $contracts = Contract::sum('total');

        $own = Contract::sum('own');

        $diff = $contracts - $expensessum;

        $categories = Category::all();

        $tcategories = Tcategory::all();

        $walletsum = WalletTotal::findOrFail(1);


        return view('welcome', compact('contracts', 'diff', 'own', 'categories', 'tcategories','expensessum', 'expensestotal', 'contractstotal', 'walletsum'));
    }
}
