<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Section;

class SectionController extends Controller
{

    public function index()
    {
        $sections = Section::all();

        return view('section.index', compact('sections'));
    }

    public function create()
    {
        return view('section.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Section::create($input);

        session()->flash('message', 'تمت اضافه القسم بنجاح ');

        return redirect('/section');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $section = Section::findOrFail($id);

        return view('section.edit', compact('section'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required | min:3'
        ]);

        $input = $request->all();

        Section::findOrFail($id)->update($input);

        session()->flash('message', 'تمت تعديل القسم بنجاح ');

        return redirect('/section');
    }

    public function destroy($id)
    {
        Section::findOrFail($id)->delete();

        return redirect('/section');
    }
}
