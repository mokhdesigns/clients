<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wallet;
use App\WalletTotal;
class WalletController extends Controller
{

    public function index()
    {
        $wallets = Wallet::all();

        return view('wallet.index', compact('wallets'));
    }

    public function create()
    {
        return view('wallet.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'from' => 'required',
            'amount' => 'required | numeric',
        ]);

        $input = $request->all();

        Wallet::create($input);

        $total  =  Wallet::sum('amount');

        WalletTotal::findOrFail('1')->update([
            'amount' => $total
        ]);

        session()->flash('message', 'تمت  الاضافه بنجاح ');

        return redirect('/wallet');
    }


    public function edit($id)
    {

      $wallet =   Wallet::findOrFail($id);

      return view('wallet.edit', compact('wallet'));    
    
    }

    public function update(Request $request, $id)
    {
        $wallet =   Wallet::findOrFail($id);

        $request->validate([
            'from' => 'required',
            'amount' => 'required | numeric',
        ]);

        $input = $request->all();

        $total  =  Wallet::sum('amount');

        WalletTotal::findOrFail('1')->update([
            'amount' => $total
        ]);
        
        $wallet->update($input);

        session()->flash('message', 'تمت  التعديل بنجاح ');

        return redirect('/wallet');
    }
    

    public function destroy($id)
    {
        Wallet::findOrFail($id)->delete();

        
        session()->flash('message', 'تمت  الحذف بنجاح ');

        return redirect('/wallet');

    }
}
