<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Transaction;
use App\Contract;

class Client extends Model
{
    protected $guarded = [];

    public function contract()
    {
        return $this->hasMany(Contract::class);
    }


    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

}
