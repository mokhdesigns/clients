<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployesTable extends Migration
{

    public function up()
    {
        Schema::create('employes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->string('name');
            $table->string('email');
            $table->integer('salary');
            $table->integer('cuts')->default(0)->nullable();
            $table->integer('bouns')->default(0)->nullable();
            $table->string('address');
            $table->string('start_date');
            $table->bigInteger('personal');
            $table->timestamps();

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('employes');
    }
}
