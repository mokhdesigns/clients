<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{

    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tcategory_id')->unsigned();
            $table->integer('amount');
            $table->string('createddate');
            $table->string('body');
            $table->timestamps();

            $table->foreign('tcategory_id')->references('id')->on('tcategories')->onDelete('cascade');
        });
    }


    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
