<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([

            'name'     => 'Super Admin',
            'email'    => 'admin@app.com',
            'password' => bcrypt('secret'),
            'avatar'   => 'logo.png',
            'role_id' => '1'
        ]);
    }
}
