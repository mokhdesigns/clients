@extends('layouts.admin')

@section('content')




            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">Data Table</h4>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">


                                        <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                            <thead>
                                            <tr>
                                                <th>اسم الموظف</th>
                                                <th>التحكم</th>
                                            </tr>
                                            </thead>


                                            <tbody>

                                                @foreach($sales as $sale )
                                            <tr>
                                                <td>{{ $sale->name }}</td>
                                                <td>
                                                        <div class="button-items">

                                                                <a href="{{ route('sale.edit', $sale->id ) }}" class="btn btn-success waves-effect waves-light">تعديل</a>

                                                                <form style="display: contents;" method="post" action="{{ route('sale.destroy', $sale->id ) }}">

                                                                        @method('DELETE')
                                                                        @csrf
                                                                        <a type="button" class="delete-btn btn btn-danger waves-effect waves-light">مسح</a>

                                                                        </form>
                                                            </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div> <!-- end col -->
                        </div> <!-- end row -->




                    </div>
                    <!-- container-fluid -->

                </div>
                <!-- content -->



@endsection
