@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">تعديل</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">العملاء</a></li>
                                        <li class="breadcrumb-item active">تعديل</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('client.update', $client->id) }}">
    @method('PUT')
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">اسم العميل</label>
                                            <div class="col-sm-6">
                                                <input class="form-control" name="name" type="text" placeholder="ادخل اسم العميل" id="example-text-input" required value="{{ $client->name }}">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label"> رقم الهاتف</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="phone" type="text" placeholder="ادخل  رقم الهاتف الخاص بالعميل" id="example-text-input" required value="{{ $client->phone }}">
                                                </div>
                                            </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">  البريد الالكتروني</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="email" type="email" placeholder="ادخل  البريد  الخاص بالعميل" id="example-text-input" required value="{{ $client->email }}">
                                                    </div>
                                                </div>

                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="تعديل">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
