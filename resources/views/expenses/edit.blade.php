@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">تعديل مصروف </h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">المصاريف</a></li>
                                        <li class="breadcrumb-item active">تعديل مصروف</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('expenses.update', $expenses->id) }}">
    @method('PUT')
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">  القسم</label>
                                                <div class="col-sm-6">
                                                        <select class="form-control" name="tcategory_id" id="example-text-input" >
                                                            @foreach ($categories as $category)
                                                            <option value="{{ $category->id }}"> {{ $category->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                            </div>


                                                <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">   التاريخ</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="createddate" type="date" placeholder="" id="example-text-input" required value="{{ $expenses->createddate }}">
                                                    </div>
                                                </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">   الملاحظات</label>
                                                    <div class="col-sm-6">
                                                        <textarea class="form-control" name="body" placeholder="اضافه ملاحظات" id="example-text-input" required></textarea>
                                                    </div>
                                                </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="تعديل المصروف">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
