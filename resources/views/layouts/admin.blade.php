<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title>{{ config('app.name', 'Where2group') }}</title>
    <meta content="Responsive admin theme build on top of Bootstrap 4" name="description" />
    <meta content="Themesdesign" name="author" />
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="stylesheet" href="{{ asset('admin/plugins/morris/morris.css') }}">
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/metismenu.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/icons.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('admin/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Cairo&display=swap" rel="stylesheet">
    <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('admin/js/metismenu.min.js') }}"></script>
    <script src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
    <script src="{{asset('charts/plugins/simplebar/js/simplebar.js')}}"></script>
    <script src="{{asset('charts/js/waves.js')}}"></script>


    <!-- Custom scripts -->
    <script src="{{asset('charts/js/app-script.js')}}"></script>
    <!--Morris JavaScript -->
    <script src="{{asset('charts/plugins/raphael/raphael-min.js')}}"></script>
    <script src="{{asset('charts/plugins/morris/js/morris.js')}}"></script>

    <!-- Charts -->



<style>
.expanded{
    cursor: pointer;

}
</style>
</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Top Bar Start -->
        <div class="topbar">

            <!-- LOGO -->
            <div class="topbar-left">
                <a href="" class="logo">
                    <span class="logo-light">
                            <i class="mdi mdi-camera-control"></i> Where2group
                        </span>
                    <span class="logo-sm">
                            <i class="mdi mdi-camera-control"></i>
                        </span>
                </a>
            </div>

            <nav class="navbar-custom">
                <ul class="navbar-right list-inline float-right mb-0">

                    <!-- full screen -->
                    <li class="dropdown notification-list list-inline-item d-none d-md-inline-block">
                        <a class="nav-link waves-effect" href="#" id="btn-fullscreen">
                            <i class="mdi mdi-arrow-expand-all noti-icon"></i>
                        </a>
                    </li>
                    <li class="dropdown notification-list list-inline-item">
                        <div class="dropdown notification-list nav-pro-img">
                            <a class="dropdown-toggle nav-link arrow-none nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <img src="{{ asset('images/' . Auth::user()->avatar ) }}" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                <!-- item-->
                                <a class="dropdown-item" href="#"><i class="mdi mdi-account-circle"></i> Profile</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="#"><i class="mdi mdi-power text-danger"></i> Logout</a>
                            </div>
                        </div>
                    </li>

                </ul>

                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left waves-effect">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                </ul>

            </nav>

        </div>
        <!-- Top Bar End -->

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <div class="slimscroll-menu" id="remove-scroll">

                <!--- Sidemenu -->
                <div id="sidebar-menu">
                    <!-- Left Menu Start -->
                    <ul class="metismenu" id="side-menu">
                        <li class="menu-title">Menu</li>
                        <li>
                            <a href="{{ url('/')}}" class="waves-effect">
                                <i class="icon-accelerator"></i> <span> الرئيسيه </span>
                            </a>
                        </li>

                        <li>
                            <a href="javascript:void(0);" class="waves-effect"><i class="icon-mail-open"></i><span> البائعين <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                            <ul class="submenu">
                                <li><a href="{{ route('sale.create')}}">اضافه بائع</a></li>
                                <li><a href="{{ route('sale.index')}}"> كل البائعين</a></li>
                            </ul>
                        </li>

                        <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="icon-mail-open"></i><span> الخدمات <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                                <ul class="submenu">
                                    <li><a href="{{ route('category.create')}}">اضافه خدمه</a></li>
                                    <li><a href="{{ route('category.index')}}"> كل الخدمات</a></li>
                                </ul>
                            </li>
                        <li>
                            <a href="javascript:void(0);" class="waves-effect"><i class="icon-paper-sheet"></i><span> العملاء <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span></a>
                            <ul class="submenu">
                                    <li><a href="{{ route('client.create')}}">اضافه عميل</a></li>
                                    <li><a href="{{ route('client.index')}}"> كل العملاء</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>  العقود<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                            <ul class="submenu">
                                    <li><a href="{{ route('contract.create')}}">اضافه عقد</a></li>
                                    <li><a href="{{ route('contract.index')}}"> كل العقود</a></li>

                            </ul>
                        </li>

                        <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>الحسابات <span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">

                                        <li>
                                                <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>المحفظه<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                                    <ul class="submenu">
                                                            <li><a href="{{ route('wallet.create') }}">إضافه مبلغ</a></li>
                                                            <li><a href="{{ route('wallet.index') }}">المحفظه </a></li>
                                                    </ul>
                                                </li>

                        <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>  بند المصاريف<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                        <li><a href="{{ route('tcategory.create')}}">اضافه بند</a></li>
                                        <li><a href="{{ route('tcategory.index')}}">  كل البنود</a></li>

                                </ul>
                            </li>

                            <li>
                                <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>  المصاريف<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
                                <ul class="submenu">
                                        <li><a href="{{ route('expenses.create')}}">اضافه مصروف</a></li>
                                        <li><a href="{{ route('expenses.index')}}">  كل المصاريف</a></li>
                                </ul>
                            </li>

                        </ul>
                    </li>

    <li>
        <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>شئون العاملين<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
        <ul class="submenu">

<li>
<a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>الاقسام<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
<ul class="submenu">
        <li><a href="{{ route('section.create') }}">إضافه قسم</a></li>
        <li><a href="{{ route('section.index') }}">كل قسم</a></li>
</ul>

</li>

<li>
    <a href="javascript:void(0);" class="waves-effect"><i class="icon-pencil-ruler"></i> <span>الموظفين<span class="float-right menu-arrow"><i class="mdi mdi-chevron-right"></i></span> </span> </a>
    <ul class="submenu">
            <li><a href="{{ route('employe.create') }}">إضافه موظف</a></li>
            <li><a href="{{ route('employe.index') }}">كل الموظفين</a></li>
    </ul>
</li>
        </ul>
</li>





                        <li>
                            <a  class="waves-effect" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i>

                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             @csrf
                         </form>><i class="icon-share"></i><span>تسجيل الخروج  </span></a>
                        </li>

                    </ul>

                </div>
                <div class="clearfix"></div>

            </div>
            <!-- Sidebar -left -->

        </div>
        <!-- Left Sidebar End -->



        @yield('content')
            <footer class="footer">
                © 2019  <span class="d-none d-sm-inline-block"> - Crafted with <i class="mdi mdi-heart text-danger"></i> by where2group</span>.
            </footer>

        </div>


    </div>



    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/jszip.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/buttons.colVis.min.js') }}"></script>

    <script src="{{ asset('admin/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('admin/js/waves.min.js') }}"></script>
    <script src="{{ asset('admin/dashboard.init.js') }}"></script>
    <script src="{{asset('charts/js/sidebar-menu.js')}}"></script>

    <script src="{{ asset('admin/js/datatables.init.js') }}"></script>

    <script src="{{ asset('admin/js/app.js') }}"></script>

    <script>
        $('.expanded').on('click', function () {

          $(this).parent().toggleClass('full-expand');

        });
      </script>

</body>
</html>
