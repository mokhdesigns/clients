@extends('layouts.admin')

@section('content')



<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">تحليل المصاريف</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">تحليل المصاريف</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">إجمالي المصاريف</a></li>
                            <li class="breadcrumb-item active"><a href="/">الرئيسيه</a></li>
                        </ol>
                    </div>
                </div> <!-- end row -->
            </div>



                  </div>
            <!-- end page-title -->
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">


                            <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                <tr>
                                        <th>الشهر</th>
                                        <th>الإجمالي</th>
                                </tr>
                                </thead>
                                <tbody>
                                        @foreach($expenses as $expens)
                                        <tr>
                                            <td>{{ $expens->months }}</td>
                                            <td> {{ $expens->sums }} </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                  <div class="card">
                    <div class="card-body">
                      <div id="line-chart"></div>
                    </div>
                  </div>
                </div>
              </div><!--End Row-->


        </div>


    </div>
    <!-- content -->

    <script>

        $(function () {
            "use strict";

            Morris.Area({
                element: 'line-chart',
                behaveLikeLine: true,
                data: [
                    @foreach($expenses as  $expens)
                     {x: '2019 Q<?php echo intval(date("m", strtotime($expens->months))); ?>', y: {{ $expens->sums}} },
                    @endforeach

                ],
                xkey: 'x',
                ykeys: ['y'],
                labels: ['Y'],
                lineColors: ['#5e72e4'],
                resize: true,
                fillOpacity: 0.8,
              });

         });

            </script>
@endsection
