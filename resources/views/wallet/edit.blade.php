@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه قسم جديد</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الاقسام</a></li>
                                        <li class="breadcrumb-item active">اضافه قسم</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('wallet.update', $wallet->id) }}">
    @method('PUT')
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">المدين </label>
                                            <div class="col-sm-6">
                                                <select class="form-control" name="from" id="example-text-input">
                                                    <option value="المكتب"  @if($wallet->from == 'المكتب') 'selected' @endif>المكتب</option>
                                                    <option value="تحويل" @if($wallet->from == 'تحويل') 'selected' @endif>تحويل</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">المبلغ </label>
                                                <div class="col-sm-6">
                                                <input class="form-control" name="amount" value="{{ $wallet->amount }}" type="number" placeholder="ادخل المبلغ " id="example-text-input">
                                                </div>
                                            </div>


                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="تعديل المبلغ ">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
