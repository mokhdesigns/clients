@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه موظف جديد</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الموظفين</a></li>
                                        <li class="breadcrumb-item active">اضافه موظف</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('employe.store') }}">
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label"> القسم</label>
                                                <div class="col-sm-6">
                                                        <select class="form-control" name="section_id" id="example-text-input" >
                                                            @foreach ($sections as $section)

                                                            <option value="{{ $section->id }}"> {{ $section->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                            </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label"> الإسم</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="name" type="text" placeholder="ادخل الأسم  " id="example-text-input" required value="{{ $employe->name }}">
                                                    </div>
                                                </div>

                                                <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">  تاريخ البدء</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="start_date" type="date" placeholder="" id="example-text-input" required>
                                                    </div>
                                                </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">   البريد الإلكتروني</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="email" type="email"  placeholder="اضافه البريد الإلكتروني" id="example-text-input" required value="{{ $employe->email }}">
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-lg row">
                                                        <label for="example-text-input" class="col-sm-2 col-form-label">   الراتب</label>
                                                        <div class="col-sm-6">
                                                            <input class="form-control" name="salary" type="text" id="example-text-input" required value="{{ $employe->salary }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-lg row">
                                                            <label for="example-text-input" class="col-sm-2 col-form-label">   الخصومات</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control" type="text" name="cuts" id="example-text-input">
                                                            </div>
                                                        </div>
                                                        <div class="form-group form-group-lg row">
                                                                <label for="example-text-input" class="col-sm-2 col-form-label">   الحوافز</label>
                                                                <div class="col-sm-6">
                                                                    <input class="form-control" type="text" name="bouns" id="example-text-input">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-lg row">
                                                                    <label for="example-text-input" class="col-sm-2 col-form-label">   العنوان</label>
                                                                    <div class="col-sm-6">
                                                                        <input class="form-control" type="text" name="address" id="example-text-input" required value="{{ $employe->address }}">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group form-group-lg row">
                                                                        <label for="example-text-input" class="col-sm-2 col-form-label">   الرقم الموظيفي</label>
                                                                        <div class="col-sm-6">
                                                                            <input class="form-control" type="number" name="personal" id="example-text-input" required>
                                                                        </div>
                                                                    </div>
                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="تعديل بيانات الموظف">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
