@extends('layouts.admin')

@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="page-title-box">
                <div class="row align-items-center">
                    <div class="col-sm-6">
                        <h4 class="page-title">الرئيسيه</h4>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Stexo</a></li>
                            <li class="breadcrumb-item active">الرئيسيه</li>
                        </ol>
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end page-title -->

            <div class="row">
                <div class="col-lg-6">
                  <div class="card">
                    <div class="card-body">
                      <div id="line-chart2"></div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                      <div class="card-body">
                        <div id="line-chart"></div>
                      </div>
                    </div>
                  </div>
              </div><!--End Row-->
            <div class="row">

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                                <a href="{{ route('showexpenses') }}" class=" waves-effect waves-light" style="
                                display: block;">
                            <div class="mini-stat-icon float-right">
                                <i class="mdi mdi-cube-outline bg-primary  text-white"></i>
                            </div>
                            <div>
                            <h5 class="font-16">  اجمالي المصاريف</h5>
                            </div>
                            <h3 class="mt-4">{{ $expensessum }}</h3>
                        </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                                <a href="{{ route('showcontract') }}" class=" waves-effect waves-light" style="
                                display: block;">
                            <div class="mini-stat-icon float-right">
                                <i class="mdi mdi-briefcase-check bg-success text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16"> اجمالي العقود</h5>
                            </div>
                            <h3 class="mt-4">{{ $contracts }}</h3>
                                </a>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div class="mini-stat-icon float-right">
                                <i class="mdi mdi-tag-text-outline bg-warning text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16"> فرق العقود والمصاريف</h5>
                            </div>
                            <h3 class="mt-4">{{ $diff }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                    <div class="card">
                        <div class="card-heading p-4">
                            <div class="mini-stat-icon float-right">
                                <i class="mdi mdi-buffer bg-danger text-white"></i>
                            </div>
                            <div>
                                <h5 class="font-16"> إجمالي المديونيه</h5>
                            </div>
                            <h3 class="mt-4">{{ $own }}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-xl-3">
                        <div class="card">
                            <div class="card-heading p-4">
                                    <a href="" class=" waves-effect waves-light" style="
                                    display: block;">
                                <div class="mini-stat-icon float-right">
                                    <i class="mdi mdi-cube-outline bg-primary  text-white"></i>
                                </div>
                                <div>
                                <h5 class="font-16">  اجمالي الخزنه</h5>
                                </div>
                                <h3 class="mt-4">{{ $walletsum->amount }}</h3>
                            </a>
                            </div>
                        </div>
                    </div>

            </div>

            <div class="expandable">
                    <span class="float-left expanded"> <i class="fa fa-plus-circle"> </i></span>
            <h2 class="text-center"> معلومات اقسام المبيعات </h2>

            <div class="row">

                @foreach ($categories  as $category)


                    <div class="col-sm-6 col-xl-3">
                        <div class="card">
                                <a href="{{ route('showsalescategoryDetails', $category->id) }}">
                            <div class="card-heading p-4">
                                <div class="mini-stat-icon float-right">
                                    <i class="mdi mdi-spa-outline bg-primary  text-white"></i>
                                </div>
                                <div>
                                    <h5 class="font-16">   {{  $category->name }}</h5>
                                </div>
                                <h3 class="mt-4">{{  $category->contract->sum('total') }}</h3>

                            </div>
                                </a>
                        </div>
                    </div>

                    @endforeach


                </div>
        </div>
<div class="expandable">
        <span class="float-left expanded"> <i class="fa fa-plus-circle"> </i></span>
<h2 class="text-center"> معلومات اقسام المصاريف </h2>

                <div class="row">

                        @foreach ($tcategories  as $tcategory)


                            <div class="col-sm-6 col-xl-3">
                                <div class="card">
                                <a href="{{ route('showcategoryDetails', $tcategory->id) }}">
                                    <div class="card-heading p-4">
                                        <div class="mini-stat-icon float-right">
                                            <i class="mdi mdi-cash-marker bg-primary  text-white"></i>
                                        </div>
                                        <div>
                                            <h5 class="font-16">   {{  $tcategory->name }}</h5>
                                        </div>
                                        <h3 class="mt-4">{{  $tcategory->expense->sum('amount') }}</h3>

                                    </div>
                                </a>
                                </div>
                            </div>

                            @endforeach


                        </div>



                    </div>
        </div>
        <!-- container-fluid -->

    </div>
    <!-- content -->
    <script>

        Morris.Area({
            element: 'line-chart',
            behaveLikeLine: true,
            data: [
                @foreach($expensestotal as  $expens)
                 {x: '2019 Q<?php echo intval(date("m", strtotime($expens->months))); ?>', y: {{ $expens->sums}} },
                @endforeach

            ],
            xkey: 'x',
            ykeys: ['y'],
            labels: ['Y'],
            lineColors: ['red'],
            resize: true,
            fillOpacity: 0.8,
          });
          Morris.Area({
            element: 'line-chart2',
            behaveLikeLine: true,
            data: [
                @foreach($contractstotal as  $expens)
                 {x: '2019 Q<?php echo intval(date("m", strtotime($expens->months))); ?>', y: {{ $expens->sums}} },
                @endforeach

            ],
            xkey: 'x',
            ykeys: ['y'],
            labels: ['Y'],
            lineColors: ['green'],
            resize: true,
            fillOpacity: 0.8,
          });

    </script>

@endsection
