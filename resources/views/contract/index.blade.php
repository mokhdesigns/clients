@extends('layouts.admin')

@section('content')

        <div class="content-page">
            <!-- Start content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="page-title-box">
                        <div class="row align-items-center">
                            <div class="col-sm-6">
                                <h4 class="page-title">العقود</h4>
                            </div>
                            <div class="col-sm-6">
                            </div>
                        </div>
                        <!-- end row -->
                    </div>
                    <!-- end page-title -->


                    <div class="row m-t-30">
                            @foreach($contracts as $contract )

                        <div class="col-xl-3 col-md-6">
                            <div class="card pricing-box mt-4">
                                <div class="pricing-icon">
                                    <i class="ti-shield bg-success"></i>
                                    <a style="float:left" href="{{ route('contract.edit', $contract->id) }}" class="btn btn-success waves-effect waves-light">تعديل</a>
                                                          
                                    <form style="display: contents;" method="post" action="{{ route('contract.destroy', $contract->id) }}">

                                            @method('DELETE')
                                            @csrf
                                            <button class="delete-btn btn btn-danger waves-effect waves-light">مسح</button>
                                    </form>
                                </div>
                                <div class="pricing-content">
                                    <div class="text-center">
                                        <h5 class="text-uppercase mt-5">{{ $contract->client->name }}</h5>
                                        <div class="pricing-plan mt-4 pt-2">
                                            <h1>{{ $contract->id }} </h1>
                                            <h4>{{ $contract->category->name  }} </h4>
                                        </div>
                                        <div class="pricing-border mt-5"></div>
                                    </div>
                                    <div class="pricing-features mt-4">
                                        <p class="font-14 mb-2">   ميعاد التسليم <i class="ti-check-box text-success mr-3"></i> {{ $contract->deadline }} </p>
                                        <p class="font-14 mb-2">   الاجمالي <i class="ti-check-box text-success mr-3"></i> {{ $contract->total }} </p>
                                        <p class="font-14 mb-2"> المتبقي  <i class="ti-check-box text-danger mr-3"></i> {{ $contract->own }} </p>
                                    </div>
                                    <div class="pricing-border mt-4"></div>
                                    <div class="mt-4 pt-3 text-center">
                                        <a href="{{ route('contract.show', $contract->id) }}" class="btn btn-success btn-lg w-100 btn-round">عرض العقد</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                @endforeach

                    </div>

                </div>
                <!-- container-fluid -->

            </div>










@endsection
