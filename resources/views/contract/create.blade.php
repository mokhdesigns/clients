@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه عقد جديد</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">العقود</a></li>
                                        <li class="breadcrumb-item active">اضافه عقد</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('contract.store') }}" enctype="multipart/form-data">
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                        <div class="form-group form-group-lg row">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">نوع الخدمه </label>
                                            <div class="col-sm-6">
                                                <select class="form-control" name="category_id" type="text">

                                                    @foreach ($categories as $category)
                                                    <option value="{{ $category->id }} "> {{$category->name }} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">اسم العميل</label>
                                                <div class="col-sm-6">
                                                    <select class="form-control" name="client_id" type="text">

                                                        @foreach ($clients as $client)
                                                        <option value="{{ $client->id }} "> {{$client->name }} </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">اسم الموظف</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control" name="sale_id" type="text">

                                                            @foreach ($sales as $sale)
                                                            <option value="{{ $sale->id }} "> {{$sale->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label"> رقم الهويه</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="personal_id" type="text" placeholder=" رقم الهويه " id="example-text-input" required>
                                                </div>
                                            </div>

                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">  قيمه العقد</label>
                                                    <div class="col-sm-6">
                                                        <input class="form-control" name="total" type="text" placeholder=" القيمه الاجماليه للعقد " id="example-text-input" required>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-lg row">
                                                        <label for="example-text-input" class="col-sm-2 col-form-label"> قيمه المقدم</label>
                                                        <div class="col-sm-6">
                                                            <input class="form-control" name="advance" type="text" placeholder="المبلغ المدفوع " id="example-text-input" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group form-group-lg row">
                                                            <label for="example-text-input" class="col-sm-2 col-form-label"> ميعاد التسليم</label>
                                                            <div class="col-sm-6">
                                                                <input class="form-control" name="deadline" type="date" placeholder=" ميعاد تسليم العميل " id="example-text-input" required>
                                                            </div>
                                                        </div>

                                                        <div class="form-group form-group-lg row">
                                                                <label for="example-text-input" class="col-sm-2 col-form-label"> اسم الدومين</label>
                                                                <div class="col-sm-6">
                                                                    <input class="form-control" name="domain" type="text" placeholder="اسم الدومين في حاله الويب " id="example-text-input">
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-lg row">
                                                                    <label for="example-text-input" class="col-sm-2 col-form-label"> صوره من العقد</label>
                                                                    <div class="col-sm-6">
                                                                        <input type="file" class="form-control" name="avatar" type="text" required >
                                                                    </div>
                                                                </div>

                                                                <div class="form-group form-group-lg row">
                                                                        <label for="example-text-input" class="col-sm-2 col-form-label"> ملاحظات</label>
                                                                        <div class="col-sm-6">
                                                                            <textarea class="form-control" name="body" type="text" placeholder="ملاحظات" id="example-text-input"></textarea>
                                                                        </div>
                                                                    </div>

                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="اضف عميل جديد">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
