@extends('layouts.admin')

@section('content')

            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">عرض العقد </h4>
                                </div>
                                <div class="col-sm-6">
                                </div>
                            </div> <!-- end row -->
                        </div>
                        <!-- end page-title -->

                        <div class="row">
                                <div class="col-12">
                                    <!-- Left sidebar -->
                                    <div class="email-leftbar card">
                                        @if ($contract->own > 0)
                                        <a href="{{ route('transaction', $contract->id  ) }}" class="btn btn-danger rounded btn-custom btn-block waves-effect waves-light">اضافه دفع </a>
                                        @endif


                                        <h6 class="m-t-30"> {{ $contract->client->name  }}</h6>
                                        <h6 class="m-t-30"> {{ $contract->deadline  }}</h6>
                                        <div class="mail-list m-t-10">
                                                <a href="#"> الاجمالي <span class="float-right">{{ $contract->total   }}</span><i class="ti-location-arrow mr-2"></i></a>
                                                <a href="#"> المقدم <span class="float-right">{{ $contract->advance   }}</span><i class="ti-location-arrow mr-2"></i></a>
                                                <a href="#"> المتبقي <span class="float-right">{{ $contract->own    }}</span><i class="ti-location-arrow mr-2"></i></a>
                                                <a href="#"> الموظف <span class="float-right">{{ $contract->sale->name    }}</span><i class="ti-location-arrow mr-2"></i></a>
                                                <a href="#"> الدومين <span class="float-right">{{ $contract->domain    }}</span><i class="ti-location-arrow mr-2"></i></a>
                                        </div>

                                    </div>
                                    <!-- End Left sidebar -->

                                    <!-- Right Sidebar -->
                                    <div class="email-rightbar mb-3">

                                        <div class="card">

                                            <div class="card-body">

                                                    <div class="media m-b-30">

                                                        <div class="media-body align-self-center">
                                                            <h3 class="font-14 m-0">ملاحظات</h3>
                                                        </div>
                                                    </div>


                                                    <blockquote class="blockquote font-13 mt-4">
                                                            <p> {{ $contract->body }} </p>
                                                        </blockquote>
                                                    <hr>

                                                    <div class="row">
                                                        <div class="col-xl-2 col-md-6">
                                                                <div class="card">
                                                                   <a href="#" class="email-img-overlay">
                                                                        <img class="card-img img-fluid" src="{{ asset('images/' . $contract->avatar) }}" alt="Card image cap">
                                                                    <div class="email-overlay text-center">
                                                                        <i class="mdi mdi-download"></i>
                                                                    </div>
                                                                   </a>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>



                                        </div>
                                        <!-- card -->


                                    </div>
                                    <!-- end Col-9 -->

                                </div>

                            </div>
                            <!-- End row -->


                            <div class="row">
                                    <div class="col-12">
                                        <div class="card m-b-30">
                                            <div class="card-body">


                                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>تاريخ الدفع </th>
                                                        <th>المبلغ</th>
                                                        <th>اسم الموظف</th>
                                                    </tr>
                                                    </thead>


                                                    <tbody>

                                                        @foreach($transactions as $item )
                                                    <tr>
                                                        <td>{{ date("Y M d", strtotime($item->created_at)) }}</td>
                                                        <td>{{ $item->amount }}</td>
                                                        <td>{{ $item->name }}</td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div> <!-- end col -->
                                </div> <!-- end row -->



                        </div>
                    <!-- container-fluid -->

                </div>


@endsection
