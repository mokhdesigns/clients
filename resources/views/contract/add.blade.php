@extends('layouts.admin')

@section('content')
            <div class="content-page">

                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">اضافه عقد جديد</h4>
                                </div>
                                <div class="col-sm-6">
                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">الرئيسيه</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:void(0);">العقود</a></li>
                                        <li class="breadcrumb-item active">اضافه عقد</li>
                                    </ol>
                                </div>
                            </div> <!-- end row -->
                        </div>

<form method="post" action="{{ route('addmoney') }}" enctype="multipart/form-data">
    @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="card m-b-30">
                                    <div class="card-body">
                                          <input type="hidden" value="{{ $id }}" name="client_id">
                                            <div class="form-group form-group-lg row">
                                                    <label for="example-text-input" class="col-sm-2 col-form-label">اسم الموظف</label>
                                                    <div class="col-sm-6">
                                                        <select class="form-control" name="sale_id" type="text">

                                                            @foreach ($sales as $sale)
                                                            <option value="{{ $sale->id }} "> {{$sale->name }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                        <div class="form-group form-group-lg row">
                                                <label for="example-text-input" class="col-sm-2 col-form-label">  المبلغ</label>
                                                <div class="col-sm-6">
                                                    <input class="form-control" name="amount" type="number" placeholder="المبلغ المدفوع " id="example-text-input" required>
                                                </div>
                                            </div>



                                        <div class="row">
                                            <div class="col-sm-2">
                                            </div>
                                                <div class="col-sm-6 col-md-offset-3">
                                                    <input class="btn-block btn btn-primary" type="submit" value="اضف عميل جديد">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @endsection
