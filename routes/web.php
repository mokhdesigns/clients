<?php

Route::middleware(['auth', 'Admin'])->group(function () {

    Route::get('/', 'HomeController@index');

    Route::resource('category', 'CategoryController');

    Route::resource('client', 'ClientController');

    Route::resource('contract', 'ContractController');

    Route::resource('sale', 'SalesController');

    Route::get('transaction/{id}','ContractController@transaction')->name('transaction');

    Route::post('addmoney','ContractController@addmoney')->name('addmoney');

    Route::resource('expenses', 'ExpensesController');

    Route::resource('tcategory', 'TcategoryController');

    Route::get('showexpenses', 'DetailsController@showexpenses')->name('showexpenses');
    Route::get('showcontract', 'DetailsController@showcontract')->name('showcontract');

    Route::get('showcategoryDetails/{id}', 'DetailsController@showcategoryDetails')->name('showcategoryDetails');

    Route::get('showsalescategoryDetails/{id}', 'DetailsController@showsalescategoryDetails')->name('showsalescategoryDetails');

    Route::resource('employe', 'EmployeController');
    Route::resource('section', 'SectionController');
    Route::resource('wallet', 'WalletController');


});


Auth::routes(['register' => false]);


